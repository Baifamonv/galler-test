import { SAVE_IMAGES, FETCH_IMAGES, DELETE_IMAGE } from 'actions/types';
const initialState = [];

export default function(state= initialState, action){
    switch(action.type){
        case SAVE_IMAGES:
            return [action.payload, ...state];

        case DELETE_IMAGE:
            const imagesWithoutChosenOne = state.filter(image => image.url !== action.payload.url);
            return imagesWithoutChosenOne;

        case FETCH_IMAGES:
            const images = action.payload.data;
            return [...state, ...images];

        default:
            return state;
    }
}
