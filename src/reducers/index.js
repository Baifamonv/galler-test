import { combineReducers } from 'redux';
import imagesReducer from 'reducers/images';

export default combineReducers({
    images: imagesReducer
});

