import React from 'react';

export default ({ modalOpened, image, closeModal }) => {
    const coverClass = modalOpened
            ? 'modal-cover modal-cover-active'
            : 'modal-cover';
    const modalContainerClass = modalOpened
            ? 'modal-container modal-container-active'
            : 'modal-container';
    return (
        <React.Fragment>
            <div className={ modalContainerClass }>
                <img
                    src={ image.url }
                    className='modal-image'
                    alt={ image.title }
                />
                <h3 className='title'>Title: { image.title }</h3>
                <div className='btn' onClick={ closeModal }>close</div>
            </div>

            <div className={ coverClass } onClick={ closeModal } />
        </React.Fragment>
    );
}

