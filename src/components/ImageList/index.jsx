import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';
import './styles.css';
import Modal from './modal';

class ImageList extends Component {
    state = {
        modalOpened: false,
        image: {},
    }
    closeModal = () => this.setState({ modalOpened: false });

    handleImageClick = image => this.setState({
        modalOpened: !this.state.modalOpened,
        image
    });

    render(){
        return (
            <div className='list-container'>
                { this.props.images.map(image => (
                    <div className='image-container' key={ image.id }>
                        <img
                            onClick={ () => this.handleImageClick(image) }
                            src={ image.thumbnailUrl }
                            className='image'
                            alt={ image.title }
                        />
                        <div
                            onClick={ () => this.props.deleteImage(image) }
                            className='delete-icon'
                        >
                            <i className="fas fa-trash-alt" />
                        </div>
                    </div>
                ))}
                <Modal { ...{
                    modalOpened: this.state.modalOpened,
                    image: this.state.image,
                    closeModal: this.closeModal,
                }}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    images: state.images
});

export default connect(mapStateToProps, actions)(ImageList);
