import React from 'react';
import './styles.css';

export default () => (
    <div className='header'>
        <h1>Gallery Amazing</h1>
        <p>There are 500 images fetched by a ajax call from this website 'https://jsonplaceholder.typicode.com/photos'. Be patient if the images don't show right away</p>
    </div>
);
