import React, { Component }from 'react';
import { connect } from 'react-redux';
import * as actions from 'actions';
import './styles.css';


class CommentBox extends Component {
    state = {
        title: '',
        imageUrl: '',
        file: '',
    }

    componentWillMount = () => this.props.fetchImages();

    titleInput = e => this.setState({ title: e.target.value });

    handleAddProfilePicture = () => {
        this.refs.uploadProfilePicture.click();
    }

    imageInput = e => {
        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file,
                imageUrl: reader.result,
            });
        };

        if (file) reader.readAsDataURL(file);
    }

    handleSubmit = e => {
        e.preventDefault();
        const { imageUrl, title } = this.state;
        if (imageUrl !== '') {
            this.props.saveImages({
                thumbnailUrl: imageUrl,
                url: imageUrl,
                title,
            });
            this.setState({
                title: '',
                imageUrl: ''
            });
        }
    }

    render(){
        return (
            <form onSubmit={ this.handleSubmit }>
                <div className='upload'>
                    <input
                        type='file'
                        id='selectedFile'
                        style={{ display: 'none' }}
                        ref="uploadProfilePicture"
                        accept='image/*'
                        onChange={ this.imageInput }
                    />
                    <div
                        htmlFor='selectedFile'
                        className='image-preview'
                        onClick={this.handleAddProfilePicture}
                        style={{ backgroundImage: `url(${ this.state.imageUrl })` }}
                    >
                        <i className='fas fa-upload' />
                    </div>
                </div>

                <input
                    autoFocus
                    type='text'
                    className='input-image-title'
                    placeholder='Give a title to upload image'
                    value={ this.state.title }
                    onChange={ this.titleInput }
                />

                <div>
                    <button className='btn'>Add your image to our amazing Gallery</button>
                </div>
            </form>
        );
    }
}

export default connect(undefined, actions)(CommentBox);
