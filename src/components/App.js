import React from 'react';
import UploadBox from 'components/UploadBox';
import ImageList from 'components/ImageList';
import Header from 'components/Header';

export default () => {
    return (
        <React.Fragment>
            <Header />
            <UploadBox />
            <ImageList />
        </React.Fragment>
    );
}
