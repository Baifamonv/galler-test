export const SAVE_IMAGES = 'SAVE_IMAGES';
export const FETCH_IMAGES = 'FETCH_IMAGES';
export const DELETE_IMAGE = 'DELETE_IMAGE';
