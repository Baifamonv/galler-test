import { SAVE_IMAGES, FETCH_IMAGES, DELETE_IMAGE } from 'actions/types';
import axios from 'axios';

export function saveImages(image){
    return {
        type: SAVE_IMAGES,
        payload: image
    }
}

export function deleteImage(image){
    return {
        type: DELETE_IMAGE,
        payload: image
    }
}

export function fetchImages(){
    const response = axios.get('https://jsonplaceholder.typicode.com/photos');
    return {
        type: FETCH_IMAGES,
        payload: response
    }
}
