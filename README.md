This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).
## reason to use this react bootstrap

I like the hot-loading feature

## Changes I made for this bootstrap

Reorgnized the whole folder structure<br>
Add redux to manage state<br/>
Add Axios to deal with Ajax call<br/>
Add .env file to add enviroment variable so that importing component become nicer 
Use icons from ifontawesome.com

## Features
Home page will load images provided from [https://jsonplaceholder.typicode.com](https://jsonplaceholder.typicode.com/photos) <br>
User could upload image by click upload image icon and see the preview. If the user doesn't like the preview, he could reclick upload icon and choose again <br/>
User could fill the title and click add image button, then the image will be automatically added to the begining of gallery <br/>
However, if the user doesn't upload any image but fills the title, no image would be uploaded<br />
User also could remove a picture by click gabage icon below each image<br/>
User also could click any image and a modal would pop out with detail infomation and big size of image instead of thumbnail<br/>

### Firstly type `run npm install` in the cmd console
It will install all the dependencies

### Then type `run npm start` in the cmd console
New browser would show up.
Runs the app in the development mode.<br>
The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
